
import sys

PAGERCODE = {
  "a": ["4", "8"],
  "b": ["8"],
  "c": ["6"],
  "d": ["0"],
  "e": ["3"],
  "f": ["5"],
  "g": ["6"],
  "h": ["4"],
  "i": ["1"],
  "j": [],
  "k": ["2"],
  "l": ["1"],
  "m": [],
  "n": [],
  "o": [],
  "p": ["9"],
  "q": [],
  "r": ["2"],
  "s": ["5"],
  "t": ["7"],
  "u": [],
  "v": [],
  "w": [],
  "x": [],
  "y": ["7", "4"],
  "z": ["2"]
}

# pagercode must be a list
def pagerCodeFor(character):
  pagerCodeList = []
  if character in PAGERCODE:
    pagerCodeList = PAGERCODE[character]
  return ''.join(pagerCodeList)

# convert to regex...
def toRegex(string):
  letters = []
  for c in string:
    letters.append(''.join([c, c.upper(), pagerCodeFor(c)]))
  
  return '['+']['.join(letters)+']'

if __name__ == "__main__":
  print(toRegex(sys.argv[1]))
