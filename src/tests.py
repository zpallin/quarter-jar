
import sys
import os
import re
import unittest

sys.path.append(os.path.abspath('./'))
import qj


class TestSwearWords(unittest.TestCase):


  def test_get_regex(self):
    swearwords = qj.SwearWords('./newSwearWords.txt')
    regex = swearwords.get_regex('f-word')
    self.assertTrue(re.match(regex, ' fuck '))
    self.assertFalse(re.match(regex, ' shit '))
    
  def test_eval_string(self):
    swearwords = qj.SwearWords('./newSwearWords.txt')
    self.assertEquals(swearwords.eval_string('fuck'), ['f-word'])
    self.assertEquals(swearwords.eval_string('shit'), ['s-word'])
    

if __name__ == '__main__':
  unittest.main()    
