
import os
import re
import json


class SwearWords:
  """
  contains a list of regex matches for words
  and functions to peruse and modify the list

  the file to import from must be in this format:
  ```
  {
    "SwearWords": {
      "nickname": "regex",
      ...
    }
  }
  ```
  """

  words = {}

  def __init__(self, importFileName):
    """imports the file containing all of the matches and names"""
    with open(importFileName, 'r') as importData:
      self.words = json.loads(importData.read())["SwearWords"]


  def eval_string(self, string):  
    """evaluates string for swear words"""
    finalList = []

    for word in self.words:
      if re.match(self.words[word], string):
        finalList.append(word)

    return finalList


  def eval_message(self, message):
    """uses message object from discord package"""
    results = self.eval_string(message.content)
    if len(results) > 0:
      print(f'{message.author} in {message.guild.name} used the following swear words:\n - {results}')
      return True

    return False

  def get_regex(self, word):
    """allows you to get the regex for a particular nickname"""
    return self.words[word]


async def add_emoji_to_guild(emoji_name, emoji_file, guild):
  """uses guild object from discord and emoji input"""
  emoji_exists = False
  for emoji in guild.emojis:
    if emoji.name == emoji_name:
      emoji_exists = True
      print(f'Emoji "{emoji_name}" already exists for guild "{guild.name}"!')
 
  if not emoji_exists:
    with open(emoji_file, 'rb') as imageFile:
      f = imageFile.read()
      await guild.create_custom_emoji(name=emoji_name, image=bytearray(f))
      print(f'Added "{emoji_name}" emoji to guild "{guild.name}"!')


def display_members(guild):
  """for displaying all members of a guild"""
  members = '\n - '.join([member.name for member in guild.members])
  print(f'Guild members:\n - {members}')


async def add_emoji_to_msg(emoji_name, message):
  """looks for emoji to add, tries to add emoji if it exists..."""
  for emoji in message.guild.emojis:
    if emoji.name == emoji_name:
      await message.add_reaction(emoji)
      return

  print(f'ERROR: could not find emoji named "{emoji_name}"')
