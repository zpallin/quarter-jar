import os
import sys
import re
import discord
sys.path.append(os.path.abspath('./src'))
import qj

FILE_PATH = os.path.dirname(os.path.realpath(__file__))
TOKEN = os.getenv('QJ_DISCORD_TOKEN')
#WORDS_FILE = os.getenv('QJ_WORDS_FILE', 'swearwords.txt')
WORDS_FILE = os.getenv('QJ_WORDS_FILE', os.path.join(FILE_PATH, 'content', 'swearwords.json'))
EMOJI_FILE = os.getenv('QJ_EMOJI_FILE', os.path.join(FILE_PATH, 'images', 'swearjar.png'))
EMOJI_NAME = os.getenv('QJ_EMOJI_NAME', 'swear_jar')
WORDS_LIST = []

# prep swearwords object
swearwords = qj.SwearWords(WORDS_FILE)

# prep client
client = discord.Client()


# on_ready 
@client.event
async def on_ready():
  print(f'{client.user} has connected to Discord!')

  # display connected guilds
  for guild in client.guilds:
    print(
      f'{client.user} is connected to the following guild:\n'
      f'{guild.name}(id: {guild.id})\n'
    )

    qj.display_members(guild)
    await qj.add_emoji_to_guild(EMOJI_NAME, EMOJI_FILE, guild)
    print("-"*10)


# on_message
@client.event
async def on_message(message):
  if message.author == client.user:       # prevents infinite loop
    return

  if swearwords.eval_message(message):
    await qj.add_emoji_to_msg(EMOJI_NAME, message)


# entrypoint
if __name__ == "__main__":
  client.run(TOKEN)
