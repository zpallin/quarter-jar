# quarter-jar

In a nutshell, this bot adds a "quarter" to a "jar" every time a user in the channel swears. 

More specifically, the application reviews all incoming messages for string matches it has in its database. If there are any matches, the message is stored in the database along with a separate record of each match that points back to the db record for the message that was caught. Then, the bot adds an emoji to the message in discord.

## Adding the bot

An existing bot runs from my desktop, if that's what you want. You can give it permission [here](https://discord.com/api/oauth2/authorize?client_id=719664070827180074&permissions=1074228288&scope=bot)
